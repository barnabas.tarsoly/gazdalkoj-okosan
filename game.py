import pygame 
import sys
import random

import time

pygame.init()
size = width, height = 1366, 720
black = 0, 0, 0
backGroundColor = 0, 200, 222
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Gazdálkodj okosan")
pygame.display.set_icon(pygame.image.load("./images/icon.ico"))
dice_model = [
        pygame.image.load("./images/dice/dice1.png"),
        pygame.image.load("./images/dice/dice2.png"),
        pygame.image.load("./images/dice/dice3.png"),
        pygame.image.load("./images/dice/dice4.png"),
        pygame.image.load("./images/dice/dice5.png"),
        pygame.image.load("./images/dice/dice6.png")
    ]

class DrawDice:
    def __init__(self, model):
        self.currentSprite = 0
        self.dice_model = model
        self.rollList = [model[0]]
        self.diceSize = model[0].get_size()
        self.diceCoordinate = (540-self.diceSize[0]/2,155)
        self.screen = screen
        self.num = 1
        self.clicked = False
        self.generated = False
        # for i, d in enumerate(self.dice_model):
        #     self.dice_model[i] = pygame.transform.scale2x(d) # double the dice size
    def generateRoll(self, num):
        self.num = num
        self.currentSprite = 0
        self.generated = True
        rollList = []
        for i in range(100):
            rollList.append(self.dice_model[random.randint(1,6)-1])
        rollList.append(self.dice_model[num-1])
        self.rollList = rollList
        return self.rollList

    def setClicked(self, tf):
        self.clicked = tf   
    
    def drawDice(self):
        # rollList = self.rollList
        # print(self.currentSprite)
        if self.currentSprite >= len(self.rollList):
            self.screen.blit(self.rollList[-1],self.diceCoordinate)
            if self.clicked == True and self.generated == True:
                self.generated = False
                return True
        else:
            self.screen.blit(self.rollList[self.currentSprite],self.diceCoordinate)
            self.currentSprite += 1
            return False

    def getCoords(self):
        return (self.diceCoordinate[0], self.diceCoordinate[1], self.diceSize[0], self.diceSize[1])    
    
    def getClicked(self, mouseCoords):
        size = self.getCoords()
        wh = (size[2],size[3])
        topx, topy = size[0], size[1]
        # self.clicked = True
            
        botx, boty= topx+wh[0], topy+wh[1]
        if topx+(botx-topx) > mouseCoords[0] > topx and topy+(boty-topy) > mouseCoords[1] > topy: 
            self.clicked = True 
            return True
    def getNum(self):
        return self.num


class DrawTable():
    def __init__(self, screen):
        self.screen = screen
        
        self.fieldsDummy = [pygame.image.load("./images/table/field.png"),pygame.image.load("./images/table/bigField.png")]
        self.fields = []
        self.fields.append("nothing")
        self.fields.append(self.fieldsDummy[0])
        self.fields.append(pygame.transform.rotate(self.fieldsDummy[0],180))
        self.fields.append(pygame.transform.rotate(self.fieldsDummy[0],-90))
        self.fields.append(pygame.transform.rotate(self.fieldsDummy[0],90))
        self.fields.append(self.fieldsDummy[1])
        self.whi = (255,255,255)
        self.yel = (255,201,14)
        self.brn = (185,122,87)
        self.grn = (34,177,76)
        self.red = (237,28,36)
        self.blu = (0,162,232)
        self.magenta = (216,52,211)
        self.orient = ["nothing", "hl", "hr","vu", "vd","big"]
        self.map = [
            [5,4,4,5,0,0,5,4,4,5],
            [2,0,0,1,0,0,2,0,0,1],
            [2,0,0,5,4,4,5,0,0,1],
            [2,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [2,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [2,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [2,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [5,3,3,3,3,3,3,3,3,3,3,3,3,5]
            
            ]

        self.rowY = [180,60,60,60,60,60,60,180]
        self.allFields = []
        self.fieldNumbers = [
            {"number": 20, "color": self.whi, "text":"Lépj a 25-ös mezőre"}, {"number": 21, "color": self.yel, "text":"Rendszeresen tisztálkodsz, jutalmul még egyszer dobhatsz"}, {"number": 22, "color": self.red,"text":"Húzz egy szerencse kártyát"}, {"number": 23, "color": self.whi,"text":"Segíted az idős embereket, jutalmul lépj a 28-as mezőre!"}, {"number": 30, "color": self.whi,"text":"Nyári táborban pihensz. Csak hatos dobással mehetsz tovább."}, 
            {"number": 31, "color": self.red, "text":"Húzz egy szerencse kártyát"}, {"number": 32, "color": self.blu, "text":"Versenyen vettél részt, ahol első helyezett lettél."}, {"number": 33, "color": self.whi, "text":"Figyelmes voltál, megállítottál egy tolvajt. Jutalmul lépj a 2-es mezőre."}, {"number": 19, "color": self.grn, "text":"Lépj előre 3 mezőt"}, {"number": 24, "color": self.blu, "text":"Az olvasás hasznos, segít fejlődni. Vásárolj könyvutalványt. Ára 2.000 Ft."}, 
            {"number": 29, "color": self.blu, "text":"IKEA"}, {"number": 34, "color": self.brn, "text":"Műszaki áruház"}, {"number": 18, "color": self.blu, "text":"Színházba és moziba mentél, a jegyek ára 8.000Ft volt."}, {"number": 25, "color": self.whi, "text":"Vidámparkban szórakoztál. A jegy ára 25.000Ft volt."}, {"number": 26, "color": self.brn, "text":"Lépj vissza a 17-es mezőre, majd dobj mégegyszer."}, 
            {"number": 27, "color": self.yel, "text":"Lépj előre egy mezőt!"}, {"number": 28, "color": self.whi, "text":"Közlekedés biztonsági versenyen nyertél 10.000Ft-ot"}, {"number": 35, "color": self.red, "text":"Húzz egy szerencse kártyát"}, {"number": 17, "color": self.brn, "text":"Műszaki áruház"}, {"number": 36, "color": self.brn, "text":"Múzeumban jártál. A belépő ára 1.000Ft volt"}, 
            {"number": 16, "color": self.yel, "text":"Ha kötöttél élet biztosítást, kapsz 50.0000Ft-ot"}, {"number": 37, "color": self.grn, "text":"Rejtvénnyel utazást nyertél. Lépj a 39-es mezőre!"}, {"number": 15, "color": self.red, "text":"Húzz egy szerencse kártyát"}, {"number": 38, "color": self.yel, "text":"Élelmiszert vásároltál, melynek ára 8.000Ft volt"}, {"number": 14, "color": self.brn, "text":"Egyszer kimaradsz a dobásból."}, 
            {"number": 39, "color": self.blu, "text":"Hétvégi pihenésed túrázással töltötted. Még egyszer dobhatsz"}, {"number": 13, "color": self.whi, "text":"IKEA"}, {"number": 12, "color": self.grn, "text":"Lépj előre két mezőt!"}, {"number": 11, "color": self.brn, "text":"Dobhatsz még egyszer!"}, {"number": 10, "color": self.yel, "text":"Lépj a 15-ös mezőre!"}, 
            {"number": 9, "color": self.red, "text":"Húzz egy szerencse kártyát!"}, {"number": 8, "color": self.grn, "text":"Köthetsz élet- és lakásbiztosítást. Az életbiztosítás ára 7.000, a lakásbiztosítást ára 12.000 Ft."}, {"number": 7, "color": self.blu, "text":"IKEA"}, {"number": 6, "color": self.brn, "text":"A dohányzás káros és drága! Ezt vésd eszedbe és ne gyűjts rá, a dohány ára 1.500Ft"}, {"number": 5, "color": self.blu, "text":"Zárd el a csapot! Egy körből kimaradsz."}, 
            {"number": 4, "color": self.brn, "text":"Műszaki Áruház"}, {"number": 3, "color": self.yel, "text":"Takarékoskodj! Betéteid után 5% kamatot kapsz."}, {"number": 2, "color": self.red, "text":"Húzz egy szerencse kártyát! "}, {"number": 1, "color": self.brn, "text":"Szemeteltél, ezért fizess 1.000 Ft-ot büntetésül!"}, {"number": 0, "color": self.blu, "text":"Start mező"}]
    def drawFields(self):
        for field in self.allFields:
            self.screen.blit(field["surface"],field["coords"])
            
    def changeColor(self,img, n_color):
        
        img_copy = pygame.Surface(img.get_size())
        
        img.set_colorkey(self.magenta)
        img_copy.fill(n_color)
        img_copy.blit(img,(0,0))
        # self.screen.blit(img_copy,(0,0))
        # print("s")
        return img_copy
    def getAllFields(self):
        y = 0
        
        for row in range(len(self.map)):
            x = 0
            for column in self.map[row]:
                if column != 0:
                    self.allFields.append({"surface":self.fields[column],"coords":(x,y), "map":column})
                    x += self.fields[column].get_size()[0]
                else:
                    x += 60
            y += self.rowY[row]
        
        for index, field in enumerate(self.allFields):
            self.allFields[index]["orient"] = self.orient[self.allFields[index]["map"]]
            self.allFields[index]["number"] = self.fieldNumbers[index]["number"]
            self.allFields[index]["color"] = self.fieldNumbers[index]["color"] 
            self.allFields[index]["text"] = self.fieldNumbers[index]["text"]
            self.allFields[index]["surface"] = self.changeColor(field["surface"],self.fieldNumbers[index]["color"])
            self.allFields[index]["wh"] = self.allFields[index]["surface"].get_size()
            orient = self.orient[self.allFields[index]["map"]]
            mid, textcoords = (), ()
            if orient == "vd":
                mid = (field["coords"][0]+(field["wh"][0]/2),field["coords"][1]+field["wh"][1]-19)
                # self.allFields[index]["midNumber"] = mid
                textcoords = (field["coords"][0]+3,field["coords"][1]+4, 56, 200)
            elif orient == "vu":
                mid = (field["coords"][0]+(field["wh"][0]/2),field["coords"][1]+5)
                textcoords = (field["coords"][0]+3,field["coords"][1]+76, 40, 200)
                # self.allFields[index]["midNumber"] = mid
            elif orient == "hl":
                mid = (field["coords"][0]+12,field["coords"][1]+(field["wh"][1]/2)-4)
                # self.allFields[index]["midNumber"] = mid
                textcoords = (field["coords"][0]+79,field["coords"][1], 40, 200)
            elif orient == "hr":
                mid = (field["coords"][0]+field["wh"][0]-12,field["coords"][1]+(field["wh"][1]/2)-4)
                # self.allFields[index]["midNumber"] = mid
                textcoords = (field["coords"][0]+4,field["coords"][1]+3, 56, 200)
            elif orient == "big":
                mid = (field["coords"][0]+15,field["coords"][1]+10)
                textcoords = (field["coords"][0]+33,field["coords"][1]+5, 144, 200)

            self.allFields[index]["midNumber"] = mid
            self.allFields[index]["textCoords"] = textcoords
        return self.allFields

class DrawCharacters():
    def __init__(self, screen, coords):
        self.screen = screen
        self.path = "./images/characters/"
        self.coords = coords
        for i, c in enumerate(coords):
            self.coords[i]["surface"] = pygame.image.load(f'{self.path}{c["color"]}.png')
            
    def drawCharacter(self, online):
        for i, character in enumerate(self.coords):
            if i in online:
                self.screen.blit(character["surface"],(character["coords"][0]-(character["surface"].get_size()[0]/2),character["coords"][1]-(character["surface"].get_size()[1])))
    def setCharacterCoords(self, player, coords):
        self.coords[player]["coords"] = coords 
        print(player, coords)
    def getCharacterSize(self, num):
        return self.coords[num]["surface"].get_size()

    def getCharacterPos(self, player):
        return self.coords[player]["coords"]

class GetFields():
    def __init__(self,allFields):
        self.allFields = allFields

    def getField(self, mouseCoords):
        for field in self.allFields:
            topx, topy = field["coords"]
            wh = field["wh"]
            botx, boty= topx+wh[0], topy+wh[1]
            if topx+(botx-topx) > mouseCoords[0] > topx and topy+(boty-topy) > mouseCoords[1] > topy:  
                return field["number"]
    
    def getPlayerField(self, coords):
        Coords = (coords[0], coords[1])
        print(Coords)
        for field in self.allFields:
            topx, topy = field["coords"]
            wh = field["wh"]
            botx, boty= topx+wh[0], topy+wh[1]
            if topx+(botx-topx) > Coords[0] > topx and topy+(boty-topy) > Coords[1] > topy:  
                return field["number"]
    def getFieldText(self, fieldNumber):
        for field in self.allFields:
            if field["number"] == fieldNumber:
                return field["text"]
    def getFieldCoords(self, number):
        pass

class RenderText():
    def __init__(self, screen, fontSize, bold):
        self.screen = screen
        
        self.font = pygame.font.SysFont("sans-serif", fontSize)
        self.font.set_bold(bold)
        # if bold:
            # self.font.set_bold(True)
        # self.color = pygame.Color('black')

    def getTextSurfaces(self, text, wh, coord, color, center=False, centerY=False):
        temp1 = []
        for word in text.split():
            temp1.append({"word":word,"size":self.font.size(word)})  
        maxw, maxh = wh
        w = 0 
        h = 0
        final = []
        coordy = 0
        temptext = ""
        lines = 0
        for word in temp1:
            w += word['size'][0]+self.font.size(" ")[0] 
            if w >= maxw:      
                w = 0
                h += word['size'][1]
                temptext = temptext[:-1]
                lines += 1
                if center:
            
                    final.append({"coords":[coord[0]-(self.font.size(temptext)[0]/2),coord[1]+coordy],'word':temptext, "color":color, "surface":self.font.render(temptext, False, color)})
                else:
                    final.append({"coords":[coord[0],coord[1]+coordy],'word':temptext, "color":color, "surface":self.font.render(temptext, False, color)})
                temptext = ""
                temptext += word["word"] + " "
                coordy+= word["size"][1]
            else:
                temptext += word["word"] + " "
        if len(temptext) >= 1:
            temptext = temptext[:-1]
            h += word['size'][1]
            lines += 1
            if center:
                
                final.append({"coords":[coord[0]-(self.font.size(temptext)[0]/2),coord[1]+coordy],'word':temptext, "color":color, "surface":self.font.render(temptext, False, color)})
            else:
                final.append({"coords":[coord[0],coord[1]+coordy],'word':temptext, "color":color, "surface":self.font.render(temptext, False, color)})
        if centerY:
            for index, line in enumerate(final):
                final[index]["coords"][1] -= h/lines
        


        return final 

    # def newTextSize(self, fontSize):
    #     self.font = pygame.font.SysFont("sans-serif", fontSize)

    def render(self, slist):
        for line in slist:
            # print(line, slist)
            self.screen.blit(line["surface"],line["coords"])
    

class Timer():
    def __init__(self, time):
        self.playtime = time +1000
        self.lastTime = 0
        self.defTime = time + 1000
    def getTime(self, ms):
        return time.strftime('%M:%S', time.gmtime(ms/1000))

    def time(self, ms):
        
        self.playtime -= (ms-self.lastTime)
        self.lastTime = ms
        # print(ms, self.playtime, self.lastTime)
        if self.playtime < 0:
            self.playtime = self.defTime

        return self.getTime(self.playtime)
timer = Timer(15000)
class TextDisplay2():
    def __init__(self):
        self.window_size = (495,114)
        self.window = pygame.Surface(self.window_size, pygame.SRCALPHA)
        self.empty_window = pygame.Surface(self.window_size, pygame.SRCALPHA)
        self.coords = (196, 420)
        self.text = ""

    def set_text(self,text):
        if self.text != text:
            self.generate_def()
        self.text_list = self.text_class.getTextSurfaces(text,self.window.get_size(),(self.window.get_size()[0]//2,self.window.get_size()[1]//2),(0,0,0), True, True)
        self.text_render = self.text_class.render(self.text_list)
    def generate_def(self):
        self.window = pygame.Surface(self.window_size, pygame.SRCALPHA)
        self.text_class = RenderText(self.window,25, True)
    def draw(self, on_field):
        if on_field:
            screen.blit(self.window, self.coords)
        else:
            screen.blit(self.empty_window, self.coords)

    def set_image(self, image_number):
        # self.image = 
        try:
            self.raw = pygame.image.load(f"./images/{image_number}.png")
            self.image = pygame.transform.scale(self.raw, (int(self.raw.get_size()[0]*0.24),int(self.raw.get_size()[1]*0.24)))
            self.window.blit(self.image, (0,360-self.image.get_size()[1]))
        except:
            self.raw = pygame.image.load(f"./images/2.png")
            self.image = pygame.transform.scale(self.raw, (int(self.raw.get_size()[0]*0.24),int(self.raw.get_size()[1]*0.24)))
            self.window.blit(self.image, (0,360-self.image.get_size()[1]))
    
class TextDisplay():
    def __init__(self):
        self.window = pygame.Surface((720,360), pygame.SRCALPHA)
        self.window.fill((53, 212, 204))
        pygame.draw.rect(self.window, (0,0,0), (0,0,720,360), 2)
        pygame.draw.rect(self.window, (0,0,0), (0,0,25,25), 2)
        self.close_button_class  = RenderText(self.window,30, True)

        self.close_button_list  = self.close_button_class.getTextSurfaces("X",(25,25), (5,5), (0,0,0))
        self.close_button_render = self.close_button_class.render(self.close_button_list)
        # self.window.blit(self.close_button_surface,(0,0))
        # self.window.draw()
        self.coords = (180,180)
        self.active = False
        self.text_class = RenderText(self.window,30, True)
        self.text = ""
    def generate_def(self):
        self.window = pygame.Surface((720,360), pygame.SRCALPHA)
        self.window.fill((53, 212, 204))
        pygame.draw.rect(self.window, (0,0,0), (0,0,720,360), 2)
        pygame.draw.rect(self.window, (0,0,0), (0,0,25,25), 2)
        self.close_button_class  = RenderText(self.window,30, True)
        self.close_button_list  = self.close_button_class.getTextSurfaces("X",(25,25), (5,5), (0,0,0))
        self.close_button_render = self.close_button_class.render(self.close_button_list)
        self.text_class = RenderText(self.window,30, True)
        # self.text = ""
    def set_text(self, text):
        if self.text != text:
            self.generate_def()
        self.text_list = self.text_class.getTextSurfaces(text,(360,720),(self.window.get_size()[0]//2,self.window.get_size()[1]//2),(0,0,0), True, True)
        self.text_render = self.text_class.render(self.text_list)

    def set_image(self, image_number):
        # self.image = 
        try:
            self.raw = pygame.image.load(f"./images/{image_number}.png")
            self.image = pygame.transform.scale(self.raw, (int(self.raw.get_size()[0]*0.24),int(self.raw.get_size()[1]*0.24)))
            self.window.blit(self.image, (0,360-self.image.get_size()[1]))
        except:
            self.raw = pygame.image.load(f"./images/2.png")
            self.image = pygame.transform.scale(self.raw, (int(self.raw.get_size()[0]*0.24),int(self.raw.get_size()[1]*0.24)))
            self.window.blit(self.image, (0,360-self.image.get_size()[1]))
    def draw_window(self):
        if self.active:
            screen.blit(self.window,self.coords)

    def close_window(self, mouseCoords):
        if self.coords[0]+((self.coords[0]+25)-self.coords[0]) > mouseCoords[0] > self.coords[0] and self.coords[1]+((self.coords[1]+25)-self.coords[1]) > mouseCoords[1] > self.coords[1]:
            self.active = False
class Luck_Card():
    def __init__(self):
        self.window = pygame.Surface((200,100), pygame.SRCALPHA)
        self.window.fill((255,201,14))
        pygame.draw.rect(self.window, (0,0,0), (0,0,200,100), 3)
        # pygame.draw.rect(self.window, (0,0,0), (0,0,25,25), 2)
        self.raw = pygame.image.load(f"./images/2.png")
        self.image = pygame.transform.scale(self.raw, (int(self.raw.get_size()[0]*0.20),int(self.raw.get_size()[1]*0.20)))
        self.window.blit(self.image, (self.window.get_size()[0]//2-self.image.get_size()[0]//2,self.window.get_size()[1]//2-self.image.get_size()[1]//2))
        self.coords = (690,430)
    def draw(self):
        screen.blit(self.window, self.coords)

    def get_clicked(self, mouseCoords):
        if self.coords[0]+((self.coords[0]+self.window.get_size()[0])-self.coords[0]) > mouseCoords[0] > self.coords[0] and self.coords[1]+((self.coords[1]+self.window.get_size()[1])-self.coords[1]) > mouseCoords[1] > self.coords[1]:
            return True
        return False

class DrawOnline:
    def __init__(self,screen):
        self.online = []
        self.screen = screen
        self.colors = ["grn", "red", "blu", "org", "whi", "yel"]
        self.images = []
        self.name_renderer = RenderText(screen, 35, True)


    def setOnline(self, online):
        self.online = online
        self.images = []
        y = 0

        for i, x in enumerate(self.online):
            self.images.append(
                {"img": pygame.image.load(f"./images/characters/{self.colors[x]}Big.png"), "color": x, "name": "", "nameSurface": [],
                 "num": i, "coords": [1080, y]})
            y += 100

    def drawCharacters(self):
        x = 1080
        y = 0

        for i, character in enumerate(self.online):

            self.screen.blit(self.images[i]["img"],self.images[i]["coords"])
            self.name_renderer.render(self.images[i]["nameSurface"])


    def setName(self, index, name):
        self.images[index]["name"] = name
        nameSurface_list = self.name_renderer.getTextSurfaces(name,size, (self.images[index]["coords"][0]+100,self.images[index]["coords"][1]+50), (0,0,0), False, True)
        self.images[index]["nameSurface"] = nameSurface_list
drawTable = DrawTable(screen)
allFields = drawTable.getAllFields()
renderText = RenderText(screen, 30, True)
drawOnline = DrawOnline(screen)
fieldNumberText = RenderText(screen, 21, False)
fieldNumberTexts = []
# fieldTexts = []
# fieldText = RenderText(screen, 18, False)
for field in allFields:
#     # print(field["midNumber"])
#     fieldTexts.append(fieldText.getTextSurfaces(field["text"], (field["textCoords"][2],field["textCoords"][3]),(field["textCoords"][0],field["textCoords"][1]), (0,0,0), True, False))
    fieldNumberTexts.append(fieldNumberText.getTextSurfaces(str(field["number"]),(200,100),field["midNumber"], (0,0,0), True))
def getPlayerDefCoords():
    playerCoords = [
        {"coords":[allFields[-1]["coords"][0]+45,allFields[-1]["coords"][1]+80], "color":"grn"},
        {"coords":[allFields[-1]["coords"][0]+90,allFields[-1]["coords"][1]+80], "color":"red"},
        {"coords":[allFields[-1]["coords"][0]+135,allFields[-1]["coords"][1]+80], "color":"blu"},
        {"coords":[allFields[-1]["coords"][0]+45,allFields[-1]["coords"][1]+140], "color":"org"},
        {"coords":[allFields[-1]["coords"][0]+90,allFields[-1]["coords"][1]+140], "color":"whi"},
        {"coords":[allFields[-1]["coords"][0]+135,allFields[-1]["coords"][1]+140], "color":"yel"}
        ]
    return playerCoords



drawCharchter = DrawCharacters(screen, getPlayerDefCoords())
drawDice = DrawDice(dice_model)
# rl = drawDice.generateRoll(random.randint(1,6))  # generate dice spin list with random number 1-6
# rl = 
getField = GetFields(allFields)
# print(allFields)
luck_card_field = Luck_Card()


text_display = TextDisplay()
text_display2 = TextDisplay2()



diceSpin = False
clock = pygame.time.Clock()
clicked_left = False
clicked_right = False
spinnedDice = False
MyPlayer = -1
currentField = 0
nextField = None
MyPlayerTurn = True
MyPlayerName = ""
main_menu = True
online_characters = [5]
char_list = [
    "a", "á", "b", "c", "d", "e", "é", "f", "g", "h", "i",
    "í", "j", "k", "l", "m", "n", "o", "ó", "ö", "ő", "p",
    "q", "r", "s", "t", "u", "ú", "ü", "ű", "w", "v", "x",
    "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"
]

class MenuCharacters():
    def __init__(self):
        self.colors = ["grn", "red", "blu", "org", "whi", "yel"]
        self.images = []
        for i, x in enumerate(self.colors):
            self.images.append({"img":pygame.image.load(f"./images/characters/{x}Big.png"), "color":x, "empty":False,"num":i})
        one_size = width // len(self.images)
        size_plus = 0
        for i, x in enumerate(self.images):
            self.images[i]["coords"] = ((one_size // 2 - x["img"].get_width() // 2) +size_plus, height // 2 - x["img"].get_height() // 2)
            size_plus += one_size


    def draw(self, screen):
        for x in self.images:
            screen.blit(x["img"],x["coords"])


    def on_mouse(self, mouseCoords):
        for i,x in enumerate(self.images):
            coords = x["coords"]
            topx = coords[0]
            topy = coords[1]
            botx = coords[0]+x["img"].get_width()
            boty = coords[1]+x["img"].get_height()
            if topx + (botx - topx) > mouseCoords[0] > topx and topy + (boty - topy) > mouseCoords[1] > topy and not x["empty"]:

                self.images[i]["img"] = pygame.image.load("./images/characters/emptyBig.png")
                self.images[i]["empty"] = True
                return x
        return False
menu_char = MenuCharacters()

player_name_display = RenderText(screen,60,True)
player_name = []
name_hint_display = RenderText(screen, 60, True)
hint_render = name_hint_display.getTextSurfaces("Írd be a neved!",size,(width//2,60),(115,115,115), True, True)


while 1:



    if main_menu:

        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()
            if event.type == pygame.KEYDOWN:
                char = pygame.key.name(event.key)
                if char in char_list:
                    if len(MyPlayerName) < 9:
                        MyPlayerName += char.upper()
                        #print(MyPlayerName)
                        player_name = player_name_display.getTextSurfaces(MyPlayerName,size,(width//2,60),(0,0,0), True, True)
                if event.key == pygame.K_BACKSPACE:
                    MyPlayerName = MyPlayerName[:-1]
                    player_name = player_name_display.getTextSurfaces(MyPlayerName, size, (width // 2, 60), (0, 0, 0), True,True)
                    #print(MyPlayerName)
        screen.fill(backGroundColor)
        if MyPlayerName == "":
            name_hint_display.render(hint_render)
        else:
            player_name_display.render(player_name)

        mousePos = pygame.mouse.get_pos()
        if pygame.mouse.get_pressed(3)[0] == 1 and clicked_left == False:
            clicked_left = True
            menu_mouse_on_character = menu_char.on_mouse(mousePos)
            if menu_mouse_on_character:
                MyPlayer = menu_mouse_on_character["num"]
                online_characters.insert(0, MyPlayer)
                drawOnline.setOnline(online_characters)
                if MyPlayerName == "":
                    MyPlayerName = f"Player{MyPlayer+1}"
                drawOnline.setName(0, MyPlayerName)
                drawOnline.setName(1, "Player2")

                main_menu = False
        if pygame.mouse.get_pressed(3)[0] == 0 and clicked_left:
            clicked_left = False
        menu_char.draw(screen)





    else:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()
            if event.type == pygame.KEYDOWN:
                # print(pygame.key.name(event.key)) # return the keys string
                if event.key == pygame.K_t:
                    print(pygame.mouse.get_pos())
                if event.key == pygame.K_d:
                    spinnedDice = False
                if event.key == pygame.K_q:
                    # print(getField.getPlayerField(drawCharchter.getCharacterPos(MyPlayer)))
                    print(MyPlayerName)
                if event.key == pygame.K_ESCAPE:
                    text_display.active = False
        mousePos = pygame.mouse.get_pos()
        if pygame.mouse.get_pressed(3)[0] == 1 and clicked_left == False:
            clicked_left = True

            if getField.getField(mousePos) != None and getField.getField(mousePos) == nextField and MyPlayerTurn and text_display.active == False:
                MyPlayerTurn = False
                playerSize = drawCharchter.getCharacterSize(0)
                drawCharchter.setCharacterCoords(MyPlayer,[mousePos[0], mousePos[1]])#+playerSize[1]/2
                print(getField.getField(pygame.mouse.get_pos()))
                currentField = getField.getPlayerField(drawCharchter.getCharacterPos(MyPlayer))

            if drawDice.getClicked(mousePos) and spinnedDice == False and MyPlayerTurn and text_display.active == False:
                drawDice.generateRoll(random.randint(1,6))
                diceSpin = True
                spinnedDice = True
            if getField.getField(mousePos) != None and text_display.active == False:
                print(getField.getFieldText(getField.getField(pygame.mouse.get_pos())))
                text_display.set_text(getField.getFieldText(getField.getField(pygame.mouse.get_pos())))
                text_display.set_image(getField.getField(pygame.mouse.get_pos()))
                text_display.active = True
            if text_display.active == False and luck_card_field.get_clicked(mousePos):
                print("clicked")

            text_display.close_window(mousePos)
        if pygame.mouse.get_pressed(3)[0] == 0 and clicked_left:
            clicked_left = False
        if pygame.mouse.get_pressed(3)[2] == 1 and clicked_right == False:
            # print("rclick")
            clicked_right = True

            for charatcterindex, character in enumerate(getPlayerDefCoords()):

                drawCharchter.setCharacterCoords(charatcterindex, character["coords"])
        if pygame.mouse.get_pressed(3)[2] == 0 and clicked_right:
            clicked_right = False
        screen.fill(backGroundColor)

        drawTable.drawFields()

        drawOnline.drawCharacters()



        for numtext in fieldNumberTexts:

            fieldNumberText.render(numtext)



        if drawDice.drawDice():
            MyPlayerTurn = True
            diceNumber = drawDice.getNum()
            drawDice.setClicked(False)
            print(diceNumber) #spin the dice
            currentField = getField.getPlayerField(drawCharchter.getCharacterPos(MyPlayer))

            nextField = currentField + diceNumber



        pygame.draw.circle(screen, (0,255,0),(945, 601), 15)
        pygame.draw.circle(screen, (255,0,0),(990, 601), 15)
        pygame.draw.circle(screen, (0,0,255),(1035, 601), 15)
        pygame.draw.circle(screen, (219,125,37),(945, 661), 15)
        pygame.draw.circle(screen, (255,255,255),(990, 661), 15)
        pygame.draw.circle(screen, (230,226,34),(1035, 661), 15)
        luck_card_field.draw()
        drawCharchter.drawCharacter(online_characters) #draw the characters
        # 536, 35
        # (169, 331)
        if getField.getField(mousePos) != None:
            text_display2.set_text(getField.getFieldText(getField.getField(mousePos)))
            text_display2.draw(True)

        else:
            text_display2.draw(False)

        renderText.render(renderText.getTextSurfaces(f"{timer.time(pygame.time.get_ticks())}", (300,100), (allFields[15]["coords"][0]+3, 70), (0,0,0),True))
        text_display.draw_window()



    # renderText.render(renderText.getTextSurfaces(f"{timer2.time(pygame.time.get_ticks())}", (300,100), (allFields[15]["coords"][0]+3, 100), (0,0,0),True))
    # fieldNumberText.render(renderText.getTextSurfaces(f"15", (300,100), (169, 331), (0,0,0),True))
    # print(round(clock.get_fps())) #show fps
    # print(pygame.time.get_ticks()) #show how much time has passed from pygame init
    pygame.display.flip() # display update
    clock.tick(60) #set fps to ~60













