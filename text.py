import pygame
pygame.init()
font = pygame.font.SysFont("Arial", 20)
font.set_bold(True)



SIZE = WIDTH, HEIGHT = (1024, 720)
FPS = 30
screen = pygame.display.set_mode(SIZE, pygame.RESIZABLE)
clock = pygame.time.Clock()


def getTextList(text):
    texts = text.split()
    temp = []
    for word in texts:
        temp.append({"word":word,"size":font.size(word)})
    return temp
text = "This is a really long sentence with a couple of breaks.\nSometimes it will break even if there isn't a break "

def getsplit(text, wh, coord, color, textsize, center=False):
    font =  pygame.font.SysFont("Arial", textsize)
    font.set_bold(True)
    texts = text.split()
    temp1 = []
    for word in texts:
        temp1.append({"word":word,"size":font.size(word)})  
    maxw, maxh = wh
    w = 0 
    final = []
    coordy = 0
    temptext = ""
    for i in temp1:
        w += i['size'][0]+font.size(" ")[0] 
        if w >= maxw:      
            w = 0
            if center == False:
                final.append({"coords":(coord[0],coord[1]+coordy),'word':temptext, "color":color, "surface":font.render(temptext, 0, color)})
            else:
                final.append({"coords":(coord[0]-(font.size(temptext)[0]/2),coord[1]+coordy),'word':temptext, "color":color, "surface":font.render(temptext, 0, color)})
            temptext = ""
            temptext += i["word"] + " "
            coordy+= i["size"][1]
        else:
            temptext += i["word"] + " "
    if len(temptext) >= 1:
        if center == False:
            final.append({"coords":(coord[0],coord[1]+coordy),'word':temptext, "color":color, "surface":font.render(temptext, 0, color)})
        else:
            final.append({"coords":(coord[0]-(font.size(temptext)[0]/2),coord[1]+coordy),'word':temptext, "color":color, "surface":font.render(temptext, 0, color)})
    return final



textlist2 = getsplit(text,(100,100),(513, 360), (255,0,0), 10, True)
# textColor = 
# print(textlist2)
while True:

    dt = clock.tick(FPS) / 1000
    screen.fill(pygame.Color('white'))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            quit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_t:
                print(pygame.mouse.get_pos())
    pygame.draw.line(screen,(0,0,0),(0,HEIGHT/2),(WIDTH,HEIGHT/2),1)
    pygame.draw.line(screen,(0,0,0),(WIDTH/2,0),(WIDTH/2,HEIGHT),1)
    for i in textlist2:
        
        screen.blit(i["surface"],i["coords"])
        # print("fut")
    # temptext = font.render("hi", True, (0,0,0))
    # screen.blit(temptext,(0,0))
    
    
    pygame.display.update()